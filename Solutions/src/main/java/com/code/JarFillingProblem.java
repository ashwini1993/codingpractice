package com.code;


import com.code.utils.Pair;

import java.util.*;

/**
 * @author ashwini.kumar
 * @version 1.0, 4/8/21 10:28 AM
 *
 *
 * Water sort puzzle game solution greedy
 */
public class JarFillingProblem {

    static class Game {
        Jar[] jars;

        public Game(Jar[] jars) {
            this.jars = jars;
        }

        public Game(Game game) {
            this.jars = new Jar[game.jars.length];
            for (int i = 0; i < game.jars.length; i++) {
                this.jars[i] = new Jar(game.jars[i]);
            }
        }

        public boolean win() {
            for(Jar jar : this.jars) {
                if(!jar.complete()) {
                    return false;
                }
            }
            return true;
        }

        public boolean isValidGame() {
            Map<Character, Integer> countMap = new HashMap<>();
            for(Jar jar : this.jars) {
                for(Mixture m : jar.mixtures) {
                    countMap.putIfAbsent(m.color, 0);
                    countMap.computeIfPresent(m.color, (k, v) -> v + m.quantity);
                }
            }
            for(Map.Entry<Character, Integer> m : countMap.entrySet()) {
                if(m.getValue() < 0 || m.getValue() > 4) {
                    System.out.println(m.getKey() + " " + m.getValue());
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            for (Jar jar : this.jars) {
                sb.append(jar).append("\n");
            }
            return sb.toString();
        }
    }

    static class Jar {
        Stack<Mixture> mixtures;
        int            size;

        public Jar(Jar jar) {
            this.mixtures = new Stack<>();
            for(Mixture m : jar.mixtures) {
                this.mixtures.push(new Mixture(m.color, m.quantity));
            }
            this.size = jar.size;
        }

        public Jar(Character[] chars) {
            this.mixtures = new Stack<>();
            for (Character c : chars) {
                if(this.mixtures.empty()){
                    this.mixtures.push(new Mixture(c, 1));
                } else {
                    Mixture m = this.mixtures.peek();
                    if(m.color == c) {
                        m.quantity += 1;
                    } else {
                        this.mixtures.push(new Mixture(c, 1));
                    }
                }
            }
            this.size = chars.length;
        }

        public boolean canAdd(Mixture mixture) {

            if (mixture == null) {
                return false;
            }
            if(mixture.quantity == 4) {
                return false;
            }
            if(this.size + mixture.quantity > 4) {
                return false;
            }
            if(!this.mixtures.empty() && this.mixtures.peek().color != mixture.color){
                return false;
            }
            return true;
        }

        public void add(Mixture mixture) {
            if (this.mixtures.empty()) {
                this.mixtures.push(mixture);
            } else {
                Mixture m = this.mixtures.peek();
                m.quantity += mixture.quantity;
            }
            this.size += mixture.quantity;
        }

        public Mixture remove() {
            if(!this.mixtures.empty()) {
                Mixture m = this.mixtures.pop();
                this.size -= m.quantity;
                return m;
            }
            return null;
        }

        public Mixture peek() {
            return this.mixtures.empty() ? null : this.mixtures.peek();
        }

        public boolean complete() {
            return this.mixtures.empty() || (this.size == 4 && this.mixtures.size() == 1 && this.mixtures.peek().quantity == 4);
        }

        public boolean colored() {
            return this.mixtures.empty() || this.mixtures.size() == 1;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("[ ");

            for (Mixture m : this.mixtures) {
                int i = 0;
                while (i++ != m.quantity) {
                    sb.append(m.color).append(" ");
                }
            }
            sb.append("]");
            return sb.toString();
        }
    }

    static class Mixture {
        char color;
        int  quantity;

        public Mixture(char color, int quantity) {
            this.color = color;
            this.quantity = quantity;
        }
    }

    static class GameSol {
        private Set<String> viewed = new HashSet<>();

        public String gameKey(Game game) {
            final StringBuilder sb = new StringBuilder("[");
            for (Jar jar : game.jars) {
                sb.append(jarKey(jar)).append("-");
            }
            sb.deleteCharAt(sb.lastIndexOf("-"));
            sb.append("]");
            return sb.toString();
        }

        public String jarKey(Jar jar) {
            final StringBuilder sb = new StringBuilder("");

            for (Mixture m : jar.mixtures) {
                int i = 0;
                while (i++ != m.quantity) {
                    sb.append(m.color);
                }
            }
            int i = 4 - jar.size;
            while (i-- != 0) {
                sb.append(" ");
            }
            return sb.toString();
        }

        public void addViewedGame(Game game) {
            this.viewed.add(gameKey(game));
        }

        public boolean isViewed(Game game) {
            return this.viewed.contains(gameKey(game));
        }

        public Integer sol(Game game, int itr, Map<Integer, Pair<String, String>> sol) {
            if (this.isViewed(game)) {
                return -1;
            }
            addViewedGame(game);
            for (int i = 0; i < game.jars.length; i++) {
                if (!game.jars[i].complete()) {
                    for (int j = 0; j < game.jars.length; j++) {
                        if (i != j) {
                            if (!(game.jars[i].colored() && game.jars[j].size == 0) && game.jars[j].canAdd(game.jars[i].peek())) {
                                Game newGame = new Game(game);
                                newGame.jars[j].add(newGame.jars[i].remove());
                                sol.put(itr, new Pair<>((i + 1) + " : " + (j + 1), gameKey(newGame)));
                                if (newGame.win()) {
                                    return itr;
                                } else {
                                    int step = sol(newGame, itr + 1, sol);
                                    if (step > -1) {
                                        return step;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return -1;
        }
    }

    static class GameOptSol {
        public String gameKey(Game game) {
            final StringBuilder sb = new StringBuilder("[");
            for (Jar jar : game.jars) {
                sb.append(jarKey(jar)).append("-");
            }
            sb.deleteCharAt(sb.lastIndexOf("-"));
            sb.append("]");
            return sb.toString();
        }

        public String jarKey(Jar jar) {
            final StringBuilder sb = new StringBuilder("");

            for (Mixture m : jar.mixtures) {
                int i = 0;
                while (i++ != m.quantity) {
                    sb.append(m.color);
                }
            }
            int i = 4 - jar.size;
            while (i-- != 0) {
                sb.append(" ");
            }
            return sb.toString();
        }

        public void print(Node node) {
            System.out.println(node);
        }

        static class Node {
            String move;
            Integer score;
            String gameKey;
            List<Node> next;

            public Node(String move, Integer score, String gameKey) {
                this.move = move;
                this.score = score;
                this.gameKey = gameKey;
                next = new LinkedList<>();
            }
        }

        Map<String, Node> viewedGames = new HashMap<>();
        int minStep = Integer.MAX_VALUE;

        public void addViewedGame(Game game, Node node) {
            this.viewedGames.put(gameKey(game), node);
        }

        public boolean isViewed(Game game) {
            return this.viewedGames.containsKey(gameKey(game));
        }

        public Node getViewedGameNode(Game game) {
            return this.viewedGames.get(gameKey(game));
        }

        public Node optSolution(Game game, int itr, Map<Integer, Pair<String, String>> sol) {
            Node node = new Node("0 : 0", 0, gameKey(game));
            optSolution(game, itr, sol, node);
            System.out.println(node);
            print(node);
            return node;
        }


        public Node optSolution(Game game, int itr, Map<Integer, Pair<String, String>> sol, Node path) {
            if (this.isViewed(game)) {
                return this.getViewedGameNode(game);
            }
            addViewedGame(game, path);
            for (int i = 0; i < game.jars.length; i++) {
                if (!game.jars[i].complete()) {
                    for (int j = 0; j < game.jars.length; j++) {
                        if (i != j) {
                            if (!(game.jars[i].colored() && game.jars[j].size == 0)) {
                                if(game.jars[j].canAdd(game.jars[i].peek())) {
                                    Game newGame = new Game(game);
                                    newGame.jars[j].add(newGame.jars[i].remove());

                                    Node nextNode = new Node((i + 1) + " : " + (j + 1), 0, gameKey(newGame));
                                    path.next.add(nextNode);
                                    sol.put(itr, new Pair<>((i + 1) + " : " + (j + 1), gameKey(newGame)));

                                    if (newGame.win()) {
                                        if(minStep > itr){
                                            minStep = itr;
                                        }
                                        return null;
                                    }
                                    if(minStep > itr) {
                                        optSolution(newGame, itr+1, sol, nextNode);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
    }

    public static void main(String[] args) {
        Game game = new Game(new Jar[] {
                new Jar(new Character[] { 'a', 'b', 'c', 'b' }),
                new Jar(new Character[] { 'd', 'e', 'f', 'a' }),
                new Jar(new Character[] { 'b', 'g', 'd', 'd' }),
                new Jar(new Character[] { 'f', 'e', 'h', 'c' }),
                new Jar(new Character[] { 'b', 'f', 'g', 'i' }),
                new Jar(new Character[] { 'e', 'h', 'g', 'd' }),
                new Jar(new Character[] { 'a', 'f', 'h', 'i' }),
                new Jar(new Character[] { 'c', 'i', 'e', 'a' }),
                new Jar(new Character[] { 'h', 'i', 'g', 'c' }),
                new Jar(new Character[] {  }),
                new Jar(new Character[] {  })
        });
        System.out.println(game.isValidGame());

        GameSol gameSol = new GameSol();
        Map<Integer, Pair<String, String>> map = new LinkedHashMap<>();
        int sol = gameSol.sol(game, 0, map);
        System.out.println(sol);
        System.out.println(gameSol.gameKey(game));
        map.forEach((k,v) -> {
            if(k<=sol) {
                System.out.println(v.snd + " <-> " + "["+ v.fst + "] - " + k);
            }
        });
        System.out.println("=============================");
        GameOptSol gameOptSol = new GameOptSol();
        Game gameOpt = new Game(game);
        Map<Integer, Pair<String, String>> map1 = new LinkedHashMap<>();
        //map1.put(-1, new Pair<>("0 : 0", gameOptSol.gameKey(gameOpt)));
        gameOptSol.optSolution(gameOpt, 0, map1);
    }
    /*
    [abcb]
    [defa]
    [bgdd]
    [fehc]
    [bfgi]
    [ehgd]
    [afhi]
    [ciea]
    [higc]
    []
    []
     */



}
