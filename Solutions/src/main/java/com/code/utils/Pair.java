package com.code.utils;

public class Pair<T,V> {

    public Pair(T fst, V snd) {
        this.fst = fst;
        this.snd = snd;
    }

    public T fst;
    public V snd;
}
